from django.core import mail
from django.test import TestCase
from django.shortcuts import resolve_url as r


class SubscribePostValid(TestCase):
    def setUp(self):
        data = dict(name='Lidiane Monteiro', cpf='12345678901',
                    email='contato.lidymonteiro@gmail.com', phone='81-997131458')
        self.client.post(r('subscriptions:new'), data)
        self.email = mail.outbox[0]

    def test_subscription_email_subject(self):
        expect = 'Confirmação de inscrição'
        self.assertEqual(expect, self.email.subject)

    def test_subscription_email_from(self):
        expect = 'contato@eventex.com.br'
        self.assertEqual(expect, self.email.from_email)

    def test_subscription_email_to(self):
        expect = ['contato@eventex.com.br', 'contato.lidymonteiro@gmail.com']
        self.assertEqual(expect, self.email.to)

    def test_subscription_email_body(self):
        contents = ['Lidiane Monteiro', '12345678901', 'contato.lidymonteiro@gmail.com', '81-997131458']
        for content in contents:
            with self.subTest():
                self.assertIn(content, self.email.body)
